﻿namespace Server

open Shared
open Saturn
open Fable.SignalR

module SignalRHub =
    open FSharp.Control.Tasks.V2

    let update (msg: Shared.Action) =
        match msg with
        | Action.IncrementCount i -> Response.NewCount(i + 1)
        | Action.DecrementCount i -> Response.NewCount(i - 1)
        | Action.RandomCharacter ->
            let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

            System.Random().Next(0,characters.Length-1)
            |> fun i -> characters.ToCharArray().[i]
            |> string
            |> Response.RandomCharacter

    let invoke (msg: Shared.Action) _ =
        task { return update msg }

    let send (msg: Shared.Action) (hubContext: FableHub<Shared.Action,Response>) =
        update msg
        |> hubContext.Clients.Caller.Send

    [<RequireQualifiedAccess>]
    module Stream =
        let sendToClient (msg: StreamFrom.Action) (hubContext: FableHub<Shared.Action,Shared.Response>) =
            match msg with
            | StreamFrom.Action.IncrementCount i ->
                seq {1..10 }
                |> FSharp.Control.AsyncSeq.ofSeq
                |> FSharp.Control.AsyncSeq.mapAsync (fun count ->
                    async {
                            do! Async.Sleep 250
                            return StreamFrom.Response.NewCount(count+i)
                    }
                )
                |> FSharp.Control.AsyncSeq.toAsyncEnum

module Router = 
    open Giraffe
    
    let mainRouting =
        choose [ GET
                 >=> choose []
                 POST
                 >=> choose []
                 PUT >=> choose []
                 DELETE >=> choose []
                 setStatusCode 404
                 >=> RequestErrors.BAD_REQUEST "Not found!" ]

    let entryRouting = router { forward "/" mainRouting }


module Program =
    open Thoth.Json.Giraffe

    let app =
        application {
            url "http://0.0.0.0:7000"
            use_router Router.entryRouting
            memory_cache
            use_static "public"
            use_json_serializer (ThothSerializer())
            use_gzip
            use_signalr (
                configure_signalr {
                    endpoint Endpoints.Root
                    send SignalRHub.send
                    invoke SignalRHub.invoke
                    stream_from SignalRHub.Stream.sendToClient
                }
            )
        }

    [<EntryPoint>]
    let main _ =
        run app
        0
