* FSharp SignalR Test

** Requirements

- Dotnet v5.0.403
- npm (used v6.14.4)

** How to run

- Open two terminals
- In the first `cd` to `Server` and run `dotnet run`
- In the other `cd` to `Client` and run `npm i && npm run start`
- Open a browser on `localhost:8080` to test a simple SignalR example
- Go to `localhost:8080/#/stream` to test a simple stream example

Note: both cases should take ~5s to start SignalR connection.
