namespace Shared

open System

[<RequireQualifiedAccess>]
type Action =
    | IncrementCount of int
    | DecrementCount of int
    | RandomCharacter

[<RequireQualifiedAccess>]
type Response =
    | NewCount of int
    | RandomCharacter of string

module Endpoints =
    let [<Literal>] Root = "/api/SignalR"

// For Stream
module StreamFrom =
    [<RequireQualifiedAccess>]
    type Action =
        | IncrementCount of int
        | DecrementCount of int
        | RandomCharacter
    
    [<RequireQualifiedAccess>]
    type Response =
        | NewCount of int
        | RandomCharacter of string

type IStreamSubscriber<'T> =
    /// Sends a new item to the client.
    abstract next: value: 'T -> unit
    /// Sends an error to the client.
    abstract error: exn option -> unit
    /// Completes the stream.
    abstract complete: unit -> unit