import { Union } from "../Client/src/fable_modules/fable-library.3.4.0/Types.js";
import { string_type, union_type, int32_type } from "../Client/src/fable_modules/fable-library.3.4.0/Reflection.js";

export class Action extends Union {
    constructor(tag, ...fields) {
        super();
        this.tag = (tag | 0);
        this.fields = fields;
    }
    cases() {
        return ["IncrementCount", "DecrementCount", "RandomCharacter"];
    }
}

export function Action$reflection() {
    return union_type("Shared.Action", [], Action, () => [[["Item", int32_type]], [["Item", int32_type]], []]);
}

export class Response extends Union {
    constructor(tag, ...fields) {
        super();
        this.tag = (tag | 0);
        this.fields = fields;
    }
    cases() {
        return ["NewCount", "RandomCharacter"];
    }
}

export function Response$reflection() {
    return union_type("Shared.Response", [], Response, () => [[["Item", int32_type]], [["Item", string_type]]]);
}

export class StreamFrom_Action extends Union {
    constructor(tag, ...fields) {
        super();
        this.tag = (tag | 0);
        this.fields = fields;
    }
    cases() {
        return ["IncrementCount", "DecrementCount", "RandomCharacter"];
    }
}

export function StreamFrom_Action$reflection() {
    return union_type("Shared.StreamFrom.Action", [], StreamFrom_Action, () => [[["Item", int32_type]], [["Item", int32_type]], []]);
}

export class StreamFrom_Response extends Union {
    constructor(tag, ...fields) {
        super();
        this.tag = (tag | 0);
        this.fields = fields;
    }
    cases() {
        return ["NewCount", "RandomCharacter"];
    }
}

export function StreamFrom_Response$reflection() {
    return union_type("Shared.StreamFrom.Response", [], StreamFrom_Response, () => [[["Item", int32_type]], [["Item", string_type]]]);
}

//# sourceMappingURL=Shared.fs.js.map
