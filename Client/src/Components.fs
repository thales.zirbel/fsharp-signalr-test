namespace App

open Feliz
open Feliz.Router
open Thoth.Fetch
open Shared
open Saturn
open Fable.SignalR
open Fable.SignalR.Feliz
open Fable.Core

type Hub = StreamHub.ServerToClient<Shared.Action,StreamFrom.Action,Shared.Response,StreamFrom.Response>

type Components =
    [<ReactComponent>]
    static member HelloWorld() =
        Html.h1 "Hello World"

    [<ReactComponent>]
    static member ExampleSignalR () =
        let textDisplay = React.functionComponent(fun (input: {| count: int; text: string |}) ->
            React.fragment [
                Html.div input.count
                Html.div input.text
            ])

        let buttons = React.functionComponent(fun (input: {| count: int; hub: Hub<Shared.Action,Response> |}) ->
            React.fragment [
                Html.button [
                    prop.text "Increment"
                    prop.onClick <| fun _ -> input.hub.current.sendNow (Action.IncrementCount input.count)
                ]
                Html.button [
                    prop.text "Decrement"
                    prop.onClick <| fun _ -> input.hub.current.sendNow (Action.DecrementCount input.count)
                ]
                Html.button [
                    prop.text "Get Random Character"
                    prop.onClick <| fun _ -> input.hub.current.sendNow Action.RandomCharacter
                ]
            ])

        let count,setCount = React.useState 0
        let text,setText = React.useState ""

        let handleMsg =
            React.useCallbackRef (fun msg -> 
                match msg with
                | Response.NewCount i -> setCount (i)
                | Response.RandomCharacter i -> setText i
            )

        let hub =
            React.useSignalR<Shared.Action,Response>(fun hub -> 
                hub.withUrl(Endpoints.Root)
                    .withAutomaticReconnect()
                    .configureLogging(LogLevel.Debug)
                    .onMessage(handleMsg)
            )

        Html.div [
            prop.children [
                textDisplay {| count = count; text = text |}
                buttons {| count = count; hub = hub |}
            ]
        ]

    [<ReactComponent>]
    static member ExampleSignalRStream () =

        let display = React.functionComponent(fun (input: {| hub: Hub |}) ->
            let count,setCount = React.useState(0)
            let subscription = React.useRef(None : System.IDisposable option)

            React.useEffectOnce(fun () -> 
                React.createDisposable <| fun () -> 
                    subscription.current |> Option.iter (fun sub -> sub.Dispose()))

            let subscriber = 
                { 
                 next = fun (msg: StreamFrom.Response) -> 
                    match msg with
                    | StreamFrom.Response.NewCount i ->
                        setCount(i)
                 complete = fun () -> JS.console.log("Complete!")
                 error = fun err -> JS.console.log(err) 
                }

            React.fragment [
                Html.div count
                Html.button [
                    prop.text "Stream From"
                    prop.onClick <| fun _ -> 
                        async {
                            let! stream = input.hub.current.streamFrom (StreamFrom.Action.IncrementCount count)
                            subscription.current <- Some (stream.subscribe(subscriber))
                        }
                        |> Async.StartImmediate
                ]
            ])

        let hub =
            React.useSignalR<Shared.Action,StreamFrom.Action,Shared.Response,StreamFrom.Response>(fun hub -> 
                hub.withUrl(Endpoints.Root)
                    .withAutomaticReconnect()
                    .configureLogging(LogLevel.Debug))

        Html.div [
            prop.children [
                display {| hub = hub |}
            ]
        ]

    [<ReactComponent>]
    static member Router() =
        let (currentUrl, updateUrl) = React.useState(Router.currentUrl())
        React.router [
            Feliz.Router.router.onUrlChanged updateUrl
            Feliz.Router.router.children [
                match currentUrl with
                | [ ] -> Components.ExampleSignalR()
                | [ "hello" ] -> Components.HelloWorld()
                | [ "stream" ] -> Components.ExampleSignalRStream()
                | otherwise -> Html.h1 "Not found"
            ]
        ]
